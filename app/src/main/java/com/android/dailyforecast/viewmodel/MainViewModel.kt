package com.android.dailyforecast.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.dailyforecast.R
import com.android.dailyforecast.database.DBHelperAction
import com.android.dailyforecast.database.WeatherEntity
import com.android.dailyforecast.entity.weather.Daily
import com.android.dailyforecast.entity.weather.Temp
import com.android.dailyforecast.entity.weather.Weather
import com.android.dailyforecast.entity.weather.WeatherModel
import com.android.dailyforecast.repository.MainRepository
import com.android.dailyforecast.utils.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(private val mainRepository: MainRepository, application: Application) :
    AndroidViewModel(application) {

    private val context: Context = application.applicationContext
    private val compositeDisposable = CompositeDisposable()
    private val weatherLiveData = MutableLiveData<Resource<WeatherModel>>()
    private val searchHistoryLiveData = MutableLiveData<Resource<List<String>>>()

    var dbHelperAction: DBHelperAction = DBHelperAction(context)

    fun fetchWeather(cityName: String) {
        // show loading
        weatherLiveData.postValue(Resource.loading(null))

        // open db
        dbHelperAction.open(true)

        // get list weather from database
        val list: List<WeatherEntity> = dbHelperAction.listWeather(cityName)

        // if list weather is existed
        if (list.isNotEmpty()) {
            // fetch data from database
            fetchFromDatabase(list)
        }
        // fetch data from API
        else {
            // call API to get lat, lng of city inputted
            compositeDisposable.add(
                mainRepository.getCity(cityName)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ cityModel ->
                        if (cityModel?.results != null && cityModel.results.isNotEmpty()) {
                            // get lat, lng for next request
                            val result = cityModel.results[0]
                            val lat: Double = result.geometry.lat
                            val lng: Double = result.geometry.lng

                            // call API to get daily forecast
                            compositeDisposable.add(
                                mainRepository.getWeather(
                                    "2ed34880c948e7e9434e5f951f96e18b",
                                    lat,
                                    lng
                                )
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({ weatherModel ->
                                        // save to database
                                        saveToDatabase(cityName, weatherModel)
                                        // post value to views
                                        weatherLiveData.postValue(Resource.success(weatherModel))
                                    }, { throwable ->
                                        weatherLiveData.postValue(throwable.message?.let {
                                            Resource.error(null, it)
                                        })
                                    })
                            )
                        } else {
                            weatherLiveData.postValue(
                                Resource.error(
                                    null,
                                    context.getString(R.string.could_not_get_lat_lng)
                                )
                            )
                        }
                    }, { throwable ->
                        weatherLiveData.postValue(throwable.message?.let {
                            Resource.error(null, it)
                        })
                    })
            )
        }
    }

    private fun saveToDatabase(cityName: String, weatherModel: WeatherModel) {
        // open db
        dbHelperAction.open(true)

        var weatherEntity: WeatherEntity?
        for (item in weatherModel.daily) {
            var icon = ""
            var description = ""

            if (item.weather.isNotEmpty()) {
                icon = item.weather[0].icon
                description = item.weather[0].description
            }

            weatherEntity = WeatherEntity(
                cityName,
                item.dt,
                item.humidity,
                item.pressure,
                item.temp.day,
                item.temp.eve,
                item.temp.min,
                item.temp.max,
                item.temp.morn,
                item.temp.night,
                description,
                icon
            )
            // add to database
            dbHelperAction.addHistory(weatherEntity)
        }
        // close db
        dbHelperAction.close()
    }

    private fun fetchFromDatabase(list: List<WeatherEntity>) {
        val dailyList: MutableList<Daily> = arrayListOf()
        for (item in list) {
            val weather = Weather(item.description, item.icon)
            val temp = Temp(item.day, item.eve, item.max, item.min, item.morn, item.night)
            val daily = Daily(item.dt, item.humidity, item.pressure, temp, listOf(weather))
            dailyList.add(daily)
        }
        // post value to views
        weatherLiveData.postValue(Resource.success(WeatherModel(dailyList)))
        // close db
        dbHelperAction.close()
    }

    fun fetchSearchHistory() {
        // open db
        dbHelperAction.open(true)
        // fetch search history from database
        val list: List<String> = dbHelperAction.listSearchHistory()
        // post back to views
        searchHistoryLiveData.postValue(Resource.success(list))
        // close db
        dbHelperAction.close()
    }

    fun getWeather(): LiveData<Resource<WeatherModel>> {
        return weatherLiveData
    }

    fun getSearchHistory(): LiveData<Resource<List<String>>> {
        return searchHistoryLiveData
    }

    override fun onCleared() {
        super.onCleared()
        // dispose resources
        compositeDisposable.dispose()
    }
}