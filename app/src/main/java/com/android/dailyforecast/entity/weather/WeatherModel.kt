package com.android.dailyforecast.entity.weather

data class WeatherModel(val daily: List<Daily>)