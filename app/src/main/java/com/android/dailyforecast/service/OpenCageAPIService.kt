package com.android.dailyforecast.service

import com.android.dailyforecast.entity.city.CityModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenCageAPIService {

    @GET("geocode/v1/json?pretty=1&no_annotations=1&no_ded%20upe=1&no_record=1&limit=1&key=64b4adc491c8476e93772284769a4005")
    fun getCity(@Query("q") cityName: String?): Observable<CityModel>
}