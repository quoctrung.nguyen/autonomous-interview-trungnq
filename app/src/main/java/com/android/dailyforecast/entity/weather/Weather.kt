package com.android.dailyforecast.entity.weather

data class Weather(val description: String, val icon: String)