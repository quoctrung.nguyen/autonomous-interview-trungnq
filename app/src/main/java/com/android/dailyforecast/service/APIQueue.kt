package com.android.dailyforecast.service

import java.util.*

class APIQueue {

    private val list: MutableList<String> = LinkedList()

    operator fun get(index: Int): String {
        return list[index]
    }

    @Synchronized
    operator fun set(controller: String, index: Int) {
        list[index] = controller
    }

    @Synchronized
    fun add(controller: String, index: Int) {
        list.add(index, controller)
    }

    @Synchronized
    fun remove(index: Int) {
        list.removeAt(index)
    }

    fun indexOf(controller: String?): Int {
        return list.indexOf(controller)
    }

    fun size(): Int {
        return list.size
    }

    @Synchronized
    fun clear() {
        list.clear()
    }
}