package com.android.dailyforecast.entity.city

data class CityModel(val results: List<Result>)