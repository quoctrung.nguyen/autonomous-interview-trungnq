package com.android.dailyforecast.database

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.android.dailyforecast.database.DBHelperUnit.TABLE_HISTORY
import java.util.*

class DBHelperAction(context: Context?) {

    private var database: SQLiteDatabase? = null
    private var helper: DBHelper? = DBHelper(context)

    fun open(rw: Boolean) {
        database = if (rw) {
            helper?.writableDatabase
        } else {
            helper?.readableDatabase
        }
    }

    fun close() {
        helper?.close()
    }

    fun addHistory(entity: WeatherEntity?): Boolean {
        if (entity == null) {
            return false
        }
        val values = ContentValues()
        values.put(DBHelperUnit.COLUMN_KEYWORD, entity.keyword)
        values.put(DBHelperUnit.COLUMN_DATE, entity.dt)
        values.put(DBHelperUnit.COLUMN_HUMIDITY, entity.humidity)
        values.put(DBHelperUnit.COLUMN_PRESSURE, entity.pressure)
        values.put(DBHelperUnit.COLUMN_DAY, entity.day)
        values.put(DBHelperUnit.COLUMN_EVE, entity.eve)
        values.put(DBHelperUnit.COLUMN_MAX, entity.max)
        values.put(DBHelperUnit.COLUMN_MIN, entity.min)
        values.put(DBHelperUnit.COLUMN_MORN, entity.morn)
        values.put(DBHelperUnit.COLUMN_NIGHT, entity.night)
        values.put(DBHelperUnit.COLUMN_DESCRIPTION, entity.description)
        values.put(DBHelperUnit.COLUMN_ICON, entity.icon)
        database!!.insert(TABLE_HISTORY, null, values)
        return true
    }

    private fun getWeatherEntity(cursor: Cursor): WeatherEntity? {
        return WeatherEntity(
            cursor.getString(0),
            cursor.getLong(1),
            cursor.getInt(2),
            cursor.getInt(3),
            cursor.getDouble(4),
            cursor.getDouble(5),
            cursor.getDouble(6),
            cursor.getDouble(7),
            cursor.getDouble(8),
            cursor.getDouble(9),
            cursor.getString(10),
            cursor.getString(11)
        )
    }

    private fun getKeyword(cursor: Cursor): String? {
        return cursor.getString(0)
    }

    @SuppressLint("Recycle")
    fun listWeather(keyword: String?): List<WeatherEntity> {
        val list: MutableList<WeatherEntity> = ArrayList()
        val cursor = database!!.query(
            TABLE_HISTORY, arrayOf(
                DBHelperUnit.COLUMN_KEYWORD,
                DBHelperUnit.COLUMN_DATE,
                DBHelperUnit.COLUMN_HUMIDITY,
                DBHelperUnit.COLUMN_PRESSURE,
                DBHelperUnit.COLUMN_DAY,
                DBHelperUnit.COLUMN_EVE,
                DBHelperUnit.COLUMN_MAX,
                DBHelperUnit.COLUMN_MIN,
                DBHelperUnit.COLUMN_MORN,
                DBHelperUnit.COLUMN_NIGHT,
                DBHelperUnit.COLUMN_DESCRIPTION,
                DBHelperUnit.COLUMN_ICON
            ),
            DBHelperUnit.COLUMN_KEYWORD + " =? ",
            arrayOf(keyword),
            null,
            null,
            null
        ) ?: return list
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            getWeatherEntity(cursor)?.let { list.add(it) }
            cursor.moveToNext()
        }
        cursor.close()
        return list
    }

    @SuppressLint("Recycle")
    fun listSearchHistory(): List<String> {
        val list: MutableList<String> = ArrayList()
        val cursor = database!!.rawQuery(
            "SELECT DISTINCT "
                    + DBHelperUnit.COLUMN_KEYWORD
                    + " FROM " + TABLE_HISTORY, null
        ) ?: return list
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            getKeyword(cursor)?.let { list.add(it) }
            cursor.moveToNext()
        }
        cursor.close()
        return list
    }

    fun clearHistory() {
        database!!.execSQL("DELETE FROM " + TABLE_HISTORY)
    }
}