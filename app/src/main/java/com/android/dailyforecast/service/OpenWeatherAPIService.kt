package com.android.dailyforecast.service

import com.android.dailyforecast.entity.weather.WeatherModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherAPIService {

    @GET("data/2.5/onecall?exclude=hourly,current,minutely,alerts&units=metric")
    fun getWeather(
        @Query("appid") appId: String,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ): Observable<WeatherModel>
}