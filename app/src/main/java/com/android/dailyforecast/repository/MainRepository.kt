package com.android.dailyforecast.repository

import com.android.dailyforecast.entity.city.CityModel
import com.android.dailyforecast.entity.weather.WeatherModel
import com.android.dailyforecast.service.APIHelper
import io.reactivex.Observable

class MainRepository(private val apiHelper: APIHelper) {

    fun getCity(cityName: String) : Observable<CityModel> = apiHelper.getCity(cityName)

    fun getWeather(appId: String, lat: Double, lon: Double) : Observable<WeatherModel> = apiHelper.getWeather(appId, lat, lon)
}