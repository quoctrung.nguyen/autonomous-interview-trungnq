package com.android.dailyforecast.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context?) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "daily-forecast.db"
        private const val DATABASE_VERSION = 1
    }

    override fun onCreate(database: SQLiteDatabase) {
        database.execSQL(DBHelperUnit.CREATE_HISTORY)
    }

    override fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}
}