package com.android.dailyforecast.utils

import android.content.Context
import android.text.TextUtils
import android.widget.Toast
import com.android.dailyforecast.R
import retrofit2.HttpException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object AppUtils {

    @JvmStatic
    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun getErrorMessage(context: Context, throwable: Throwable): String {
        var errorMessage: String?

        // if non-200 response
        if (throwable is HttpException) {

            // get error Response from HttpException
            val errorResponse = throwable.response()

            // default to http status reason
            errorMessage = if (errorResponse != null) {
                errorResponse.message()
            } else {
                context.getString(R.string.network_unknown_message)
            }
            // if has errorBody
            if (errorResponse?.errorBody() != null) {
                try {
                    // set to errorMessage if not empty
                    val errorBodyMessage = errorResponse.errorBody()!!.string()
                    if (!TextUtils.isEmpty(errorBodyMessage)) {
                        errorMessage = errorBodyMessage
                    }
                } catch (ignored: IOException) {
                }
            }
        } else {
            // default from throwable message
            errorMessage = throwable.message!!
        }
        return errorMessage!!
    }

    fun getDate(milliseconds: Long, dateFormat: String): String {
        val formatter = SimpleDateFormat(dateFormat, Locale.ENGLISH)
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeZone = TimeZone.getTimeZone("Asia/Ho_Chi_Minh")
        calendar.timeInMillis = milliseconds
        return formatter.format(calendar.time)
    }
}