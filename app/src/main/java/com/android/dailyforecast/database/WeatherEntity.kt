package com.android.dailyforecast.database

data class WeatherEntity(
    val keyword: String,
    // Daily
    val dt: Long,
    val humidity: Int,
    val pressure: Int,

    // Temp
    val day: Double,
    val eve: Double,
    val max: Double,
    val min: Double,
    val morn: Double,
    val night: Double,

    // Weather
    val description: String,
    val icon: String
)
