package com.android.dailyforecast.database

object DBHelperUnit {

    const val TABLE_HISTORY = "HISTORY"

    const val COLUMN_KEYWORD = "KEYWORD"

    const val COLUMN_DATE = "DATE"
    const val COLUMN_HUMIDITY = "HUMIDITY"
    const val COLUMN_PRESSURE = "PRESSURE"

    const val COLUMN_DAY = "DAY"
    const val COLUMN_EVE = "EVE"
    const val COLUMN_MAX = "MAX"
    const val COLUMN_MIN = "MIN"
    const val COLUMN_MORN = "MORN"
    const val COLUMN_NIGHT = "NIGHT"

    const val COLUMN_DESCRIPTION = "DESCRIPTION"
    const val COLUMN_ICON = "ICON"

    const val CREATE_HISTORY = ("CREATE TABLE "
            + TABLE_HISTORY
            + " ("
            + " " + COLUMN_KEYWORD + " text,"
            + " " + COLUMN_DATE + " long,"
            + " " + COLUMN_HUMIDITY + " integer,"
            + " " + COLUMN_PRESSURE + " integer,"
            + " " + COLUMN_DAY + " double,"
            + " " + COLUMN_EVE + " double,"
            + " " + COLUMN_MAX + " double,"
            + " " + COLUMN_MIN + " double,"
            + " " + COLUMN_MORN + " double,"
            + " " + COLUMN_NIGHT + " double,"
            + " " + COLUMN_DESCRIPTION + " text,"
            + " " + COLUMN_ICON + " text"
            + ")")

    @set:Synchronized
    var entity: WeatherEntity? = null
}
