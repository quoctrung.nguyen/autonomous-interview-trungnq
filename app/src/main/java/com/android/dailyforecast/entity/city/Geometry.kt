package com.android.dailyforecast.entity.city

data class Geometry(val lat: Double, val lng: Double)