package com.android.dailyforecast.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.dailyforecast.R
import com.android.dailyforecast.entity.weather.Daily
import com.android.dailyforecast.utils.AppUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_item_list.view.*
import kotlin.math.roundToInt

class WeatherAdapter(private var dailyList: List<Daily>) :
    RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        return WeatherViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_item_list, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dailyList.size
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bind(dailyList[position])
    }

    fun setDailyList(dailyList: List<Daily>) {
        this.dailyList = dailyList
        notifyDataSetChanged()
    }

    class WeatherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mContext: Context = itemView.context

        fun bind(daily: Daily) {
            itemView.apply {
                date.text = mContext.getString(R.string.date_s).format(AppUtils.getDate(daily.dt * 1000L, "EEE, dd MMM yyyy"))

                val value: Double =
                    (daily.temp.day + daily.temp.min + daily.temp.max + daily.temp.night + daily.temp.eve + daily.temp.morn) / 6
                average_temp.text = mContext.getString(R.string.average_temperature_s).format(value.roundToInt())

                pressure.text = mContext.getString(R.string.pressure_s).format(daily.pressure)
                humidity.text = mContext.getString(R.string.humidity_s).format(daily.humidity)

                if (daily.weather.isNotEmpty()) {
                    Glide.with(mContext).load("http://openweathermap.org/img/wn/" + daily.weather[0].icon + "@2x.png").into(icon)
                    description.text = mContext.getString(R.string.description_s)
                        .format(daily.weather[0].description)
                }
            }
        }
    }
}