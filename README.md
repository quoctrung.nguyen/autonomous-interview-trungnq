
1. Provide the UX/UI searching/history search forecast screen (Done)

2. Caching the result of the search to prevent requests to the server many times. (Done)
=> I used SQLite Database to store the result of the search. 
If the result is existed in local database, we will fetch from the local database.
Else we will fetch from the API.

3. If the network is not available, the application will auto request the query right after the
network back. (Done)

4. Setup project able to build with 3 environment development, staging, production. (Done)

5. Encrypt your configuration to prevent getting the server configuration(like appId). (Not yet)
=> On Android and Server should have same encoding and decoding algorithm.
So for now, I don't know how to implement this one. It is not completed.
