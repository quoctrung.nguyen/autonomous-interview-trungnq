package com.android.dailyforecast.entity.weather

data class Daily(val dt: Long, val humidity: Int, val pressure: Int, val temp: Temp, val weather: List<Weather>)