package com.android.dailyforecast.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.dailyforecast.R
import kotlinx.android.synthetic.main.layout_item_search_history.view.*

class SearchHistoryAdapter(private var list: List<String>, private val listener: Listener) :
    RecyclerView.Adapter<SearchHistoryAdapter.SearchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        return SearchViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_item_search_history, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(list[position], listener)
    }

    fun setHistoryList(list: List<String>) {
        this.list = list
        notifyDataSetChanged()
    }

    fun getItem(position: Int)  = list[position]

    class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(string: String, listener: Listener) {
            itemView.apply {
                keyword.text = string
            }
            itemView.setOnClickListener {
                listener.onSearchHistoryClick(adapterPosition)
            }
        }
    }

    interface Listener {
        fun onSearchHistoryClick(position: Int)
    }
}