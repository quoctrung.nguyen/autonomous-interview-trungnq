package com.android.dailyforecast.utils

enum class Status {
    LOADING, SUCCESS, ERROR
}