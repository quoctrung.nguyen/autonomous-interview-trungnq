package com.android.dailyforecast.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.dailyforecast.R
import com.android.dailyforecast.adapter.SearchHistoryAdapter
import com.android.dailyforecast.adapter.WeatherAdapter
import com.android.dailyforecast.entity.weather.Daily
import com.android.dailyforecast.service.APIHelper
import com.android.dailyforecast.service.RetrofitBuilder
import com.android.dailyforecast.utils.AppUtils
import com.android.dailyforecast.utils.ConnectionType
import com.android.dailyforecast.utils.NetworkMonitorUtil
import com.android.dailyforecast.utils.Status
import com.android.dailyforecast.viewmodel.MainViewModel
import com.android.dailyforecast.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), SearchHistoryAdapter.Listener {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: WeatherAdapter
    private var list: List<Daily> = arrayListOf()
    private lateinit var searchHistoryAdapter: SearchHistoryAdapter
    private var listSearchHistory: List<String> = arrayListOf()

    private val networkMonitor = NetworkMonitorUtil(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViewModel()
        setupUI()
        setupObservers()
        setupNetworkListener()
    }

    override fun onStart() {
        super.onStart()
        networkMonitor.register()
    }

    override fun onStop() {
        super.onStop()
        networkMonitor.unregister()
    }

    private fun setupNetworkListener() {
        networkMonitor.result = { isAvailable, type ->
            runOnUiThread {
                when (isAvailable) {
                    true -> {
                        when (type) {
                            ConnectionType.Wifi -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Wifi Connection")
                                if (input.text.toString().isNotEmpty()) {
                                    viewModel.fetchWeather(input.text.toString())
                                }
                            }
                            ConnectionType.Cellular -> {
                                Log.i("NETWORK_MONITOR_STATUS", "Cellular Connection")
                                if (input.text.toString().isNotEmpty()) {
                                    viewModel.fetchWeather(input.text.toString())
                                }
                            }
                            else -> {
                            }
                        }
                    }
                    false -> {
                        Log.i("NETWORK_MONITOR_STATUS", "No Connection")
                    }
                }
            }
        }
    }

    private fun setupObservers() {
        // fetch search history
        viewModel.fetchSearchHistory()
        viewModel.getSearchHistory().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    listSearchHistory = it.data ?: arrayListOf()
                    searchHistoryAdapter.setHistoryList(listSearchHistory)
                    // if has search history
                    if (listSearchHistory.isNotEmpty()) {
                        // show search history
                        recycler_view_history.visibility = View.VISIBLE
                    } else {
                        // hide search history
                        recycler_view_history.visibility = View.GONE
                    }
                }
                else -> {
                    recycler_view_history.visibility = View.GONE
                }
            }
        })

        search.setOnClickListener {
            viewModel.fetchWeather(input.text.toString())
        }

        input.addTextChangedListener {
            // if input is empty
            if (input.text.toString().isEmpty()) {
                // re-fetch search history
                viewModel.fetchSearchHistory()
                // show search history
                recycler_view_history.visibility = View.VISIBLE
                // hide daily forecast
                recycler_view_forecast.visibility = View.GONE
            } else {
                // hide search history
                recycler_view_history.visibility = View.GONE
            }
        }

        viewModel.getWeather().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    // show daily forecast
                    recycler_view_forecast.visibility = View.VISIBLE
                    // hide progress bar
                    progress_bar.visibility = View.GONE
                    list = it.data?.daily ?: arrayListOf()
                    adapter.setDailyList(list)
                }
                Status.ERROR -> {
                    // hide daily forecast
                    recycler_view_forecast.visibility = View.GONE
                    // hide progressbar
                    progress_bar.visibility = View.GONE
                    it.message?.let { message -> AppUtils.showToast(this, message) }
                }
                Status.LOADING -> {
                    // show progressbar
                    progress_bar.visibility = View.VISIBLE
                    // hide daily forecast
                    recycler_view_forecast.visibility = View.GONE
                }
            }
        })
    }

    private fun setupUI() {
        // daily forecast
        recycler_view_forecast.layoutManager = LinearLayoutManager(this)
        adapter = WeatherAdapter(list)
        recycler_view_forecast.adapter = adapter

        // search history
        recycler_view_history.layoutManager = LinearLayoutManager(this)
        recycler_view_history.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
        searchHistoryAdapter = SearchHistoryAdapter(listSearchHistory, this)
        recycler_view_history.adapter = searchHistoryAdapter
    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(
                APIHelper(
                    RetrofitBuilder.openCageAPIService,
                    RetrofitBuilder.openWeatherAPIService
                ), application
            )
        ).get(MainViewModel::class.java)
    }

    // Call when user click on search history
    override fun onSearchHistoryClick(position: Int) {
        val keyword = searchHistoryAdapter.getItem(position)
        // hide search history
        recycler_view_history.visibility = View.GONE
        input.setText(keyword)
        viewModel.fetchWeather(keyword)
    }
}