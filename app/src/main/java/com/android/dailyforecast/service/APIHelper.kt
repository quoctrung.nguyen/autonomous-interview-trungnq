package com.android.dailyforecast.service

import com.android.dailyforecast.entity.city.CityModel
import com.android.dailyforecast.entity.weather.WeatherModel
import io.reactivex.Observable

class APIHelper(
    private val openCageAPIService: OpenCageAPIService,
    private val openWeatherAPIService: OpenWeatherAPIService
) {

    fun getCity(cityName: String): Observable<CityModel> = openCageAPIService.getCity(cityName)

    fun getWeather(appId: String, lat: Double, lon: Double) : Observable<WeatherModel> = openWeatherAPIService.getWeather(appId, lat, lon)
}