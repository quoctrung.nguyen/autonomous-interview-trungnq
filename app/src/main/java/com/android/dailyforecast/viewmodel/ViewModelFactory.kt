package com.android.dailyforecast.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.dailyforecast.repository.MainRepository
import com.android.dailyforecast.service.APIHelper

class ViewModelFactory(private val apiHelper: APIHelper, private val application: Application) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(MainRepository(apiHelper), application) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}