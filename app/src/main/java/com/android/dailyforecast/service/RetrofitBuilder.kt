package com.android.dailyforecast.service

import com.android.dailyforecast.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitBuilder {

    private const val OPEN_CAGE_DATA_URL = "https://api.opencagedata.com/"
    private const val OPEN_WEATHER_MAP_URL = "https://api.openweathermap.org/"

    private fun getOpenCageRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(OPEN_CAGE_DATA_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(getOkHttpClient())
            .build()
    }

    private fun getOpenWeatherRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(OPEN_WEATHER_MAP_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(getOkHttpClient())
            .build()
    }

    val openCageAPIService: OpenCageAPIService =
        getOpenCageRetrofit().create(OpenCageAPIService::class.java)
    val openWeatherAPIService: OpenWeatherAPIService =
        getOpenWeatherRetrofit().create(OpenWeatherAPIService::class.java)

    private fun getOkHttpClient(): OkHttpClient {
        // logging Interceptor when not PRODUCTION
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        // create OkHttpClient for adding interceptors
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build()
    }
}