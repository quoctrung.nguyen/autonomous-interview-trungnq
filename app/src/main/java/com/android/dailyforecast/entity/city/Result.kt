package com.android.dailyforecast.entity.city

data class Result(val geometry: Geometry)